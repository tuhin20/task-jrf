<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(){
        $tasks = Task::orderBy('id', 'desc')->get();
         return view('task.index',compact('tasks'));
    }
    public function storeOrUpdate(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
             'description' => 'nullable|string',
             'status' => 'nullable|string',
        ]);

        // Check if the request contains a task ID for updating
        $taskId = $request->input('id');
// dd($taskId);
        // Update existing task or create a new one
        $tasks = Task::updateOrCreate(
            ['id' => $taskId], // Search condition
            $validatedData // Data to update or create
        );

        // Optionally, you can return a response or redirect
        return response()->json(['message' => 'task created or updated successfully', 'task' => $tasks]);
    }
    public function edit($id)
    {
        // dd($id);
        $tasks = Task::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $tasks]);

    }

   
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
         $task = Task::find($id);
        if (!$task) {
            return response()->json(['message' => 'task not found.'], 404);
        }
        $task->delete();
        return response()->json(['message' => 'task deleted successfully.']);
    }
}
