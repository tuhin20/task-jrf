@extends('layout.master')
@section('content')
    <div class="container">
        <h1>Task Add</h1>
        <div class="row">
            <div class="col-md-4 card mr-2">
                <form id="taskAddForm" class="p-5">
                    @csrf
                    <!-- Hidden input field for product_id -->
                    <input type="hidden" name="id" id="task_id" value="">
                    <div class="mb-4 row">
                        <label for="TaskName" class="">Task Title</label>
                        <input type="text" name="title" class="form-control" id="title"
                            placeholder="Enter Task title">
                    </div>
                    <div class="mb-4 row">
                        <label for="description" class="">Task Description</label>

                        <input name="description" type="text" class="form-control" id="description"
                            placeholder="Enter Task Description">
                    </div>
                    <div class="mb-4 row">
                        <label for="taskStatus" class="">Task Status</label>
                        {{-- <input type="text" name="status" class="form-control" id="status"
                            placeholder="Enter Task Status"> --}}
                            <select name="status" class="form-control" id="status">
                                <option value="">Select</option>
                                <option value="Active">Active</option>
                                <option value="Pending">Pending</option>
                            </select>

                    </div>
                    <div class="mb-4 row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary px-5 float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
           <div class="col-md-7 card">
                <div class="___table p-2">
                    <div class="table-responsive scrollbar">
                        <table class="table table-bordered">
                            <thead class="table-active">
                                <tr>
                                    <th scope="col">Task Title</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Status</th>
                                    <th class="text-end" scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="taskList">
                                @foreach ($tasks as $row)
                                    <tr>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->description }}</td>
                                        <td>{{ $row->status }}</td>
                                        <td class="text-end">
                                            <button class="btn btn-primary edit-task"
                                                data-task-id="{{ $row->id }}">Edit</button>
                                            <button class="btn btn-danger delete-task"
                                                data-task-id="{{ $row->id }}">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            // Event listener for edit-taskId button click
            $('.edit-task').click(function() {
                var taskId = $(this).data('task-id');
                // alert(task);
                // Fetch task data using AJAX
                $.ajax({
                    type: 'get',
                    url: "{{ url('task/edit') }}" + '/' + taskId,
                    // data: formData,
                    success: function(response) {
                        // Handle success response
                        // console.log(response.data);
                        $('#task_id').val(response.data.id);
                        $('#title').val(response.data.title);
                        $('#description').val(response.data.description);
                        $('#status').val(response.data.status);
                        // alert(response.message);
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save Task.');
                    }
                });
                // Fill the form fields with product data

            });

            // Listen for the form submission
            $('#taskAddForm').submit(function(event) {
                event.preventDefault();
                // Serialize form data
                var formData = $(this).serialize();
                // Perform AJAX call
                $.ajax({
                    type: 'POST',
                    url: "{{ url('task/storeOrUpdate') }}",
                    data: formData,
                    success: function(response) {
                        // Handle success response
                        alert(response.message);
                        // Redirect to the task add page
                        window.location.href = '/';
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save task.');
                    }
                });
            });

            // Event listener for delete-product button click
            $('.delete-task').click(function(event) {
                event.preventDefault();
                var productId = $(this).data('task-id');
                // Send AJAX request to delete the product
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('task/delete') }}/" + productId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        // Handle success response
                        alert('task deleted successfully.');
                        // Reload the page to update the task list
                        location.reload();
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to delete task.');
                    }
                });
            });
        });
    </script>
@endsection
