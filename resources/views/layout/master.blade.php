<!DOCTYPE html>
<html lang="en">
@include('layout.style')
<body>
    @yield('content')
    <!-- jQuery -->
    @include('layout.js')
    @yield('js')
    <!-- Your custom JavaScript code --> 
</body>
</html>
