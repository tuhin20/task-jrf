<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [TaskController::class, 'index']);
Route::prefix('task')->group(function () {
    Route::post('/storeOrUpdate', [TaskController::class, 'storeOrUpdate']);
    Route::get('/edit/{id}', [TaskController::class, 'edit'])->name('task.edit');
    Route::delete('/delete/{id}', [TaskController::class, 'destroy'])->name('task.destroy');
 });
